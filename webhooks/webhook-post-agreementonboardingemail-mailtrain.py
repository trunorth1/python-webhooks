from flask import escape
from requests import get
import requests
import json
import threading
import time
import random
import string


def get_deal_data(dealID):
    data = {"dealID" : dealID}
    write_url = 'https://us-central1-repair-facilities-information.cloudfunctions.net/get-deal-data-with-dealID'
    print(write_url)
    payload = data
    headers = {'Content-Type': 'application/json'}
    dealData = requests.post(write_url, data=json.dumps(payload), headers=headers)
    dealData = dealData.json()
    return dealData

def get_random_alphanumeric_string(length):
    letters_and_digits = string.ascii_letters + string.digits
    result_str = ''.join((random.choice(letters_and_digits) for i in range(length)))
    print("Random alphanumeric String is:", result_str)
    result_str = result_str
    return result_str

def get_contact_data(contactID):
    data = {"contactID" : contactID}
    write_url = 'https://us-central1-repair-facilities-information.cloudfunctions.net/get_contact_with_contactID'
    print(write_url)
    payload = data
    headers = {'Content-Type': 'application/json'}
    contactData = requests.post(write_url, data=json.dumps(payload), headers=headers)
    contactData = contactData.json()
    return contactData

def prefill_ia_form(dealID):
    data = {"dealID" : dealID}
    write_url = 'https://us-central1-repair-facilities-information.cloudfunctions.net/prepopulate-ia-url'
    print(write_url)
    payload = data
    headers = {'Content-Type': 'application/json'}
    resolved = requests.post(write_url, data=json.dumps(payload), headers=headers)
    urlData = resolved.json()
    print(urlData)
    return urlData

def decode_mailtrain_lists(qualifiedProgram, retailerID):
    #Define Blank decodedID variable
    decodedJSON = {}

    #Set MailTrain List ID based on Qualified Program variable on Hubspot Deal
    if (qualifiedProgram == "All Inclusive") and (retailerID == "XX101"):
        decodedJSON['id'] = '67xjEbFxD'
        decodedJSON['url'] = 'https://letter.cloud.hq.trunorthdigital.com/archive/U9poecx-Q/67xjEbFxD/Xj4eWSb7r?track=no'
    elif qualifiedProgram == "All Inclusive":
        decodedJSON['id'] = '0Ip0cfZFr'
        decodedJSON['url'] = 'https://letter.cloud.hq.trunorthdigital.com/archive/yRWtZ18nv/0Ip0cfZFr/cG2K8K0tI?track=no'
    elif (qualifiedProgram == "MTW") and (retailerID == "XX101"):
        decodedJSON['id'] = 'wRPhe_Dua'
        decodedJSON['url'] = 'https://letter.cloud.hq.trunorthdigital.com/archive/xDbL430p3/wRPhe_Dua/FYqF2eJZ7?track=no'
    elif qualifiedProgram == "MTW":
        decodedJSON['id'] = 'EFUKU-dhL'
        decodedJSON['url'] = 'This Email is not configured for Referrals'
    elif (qualifiedProgram == "OEM2") and (retailerID == "XX101"):
        decodedJSON['id'] = 'NYeTf568e'
        decodedJSON['url'] = 'https://letter.cloud.hq.trunorthdigital.com/archive/WCNAHK3lL/NYeTf568e/FeDtmlo8p?track=no'
    elif qualifiedProgram == "OEM2":
        decodedJSON['id'] = 'G-MqR0REW'
        decodedJSON['url'] = 'https://letter.cloud.hq.trunorthdigital.com/archive/j84Qezrt6/G-MqR0REW/cavDipPnD?track=no'
    elif (qualifiedProgram == "Super Annuated Heavy D") and (retailerID == "XX101"):
        decodedJSON['id'] = 'zNCjnb1Qf'
        decodedJSON['url'] = 'https://letter.cloud.hq.trunorthdigital.com/archive/BzJouxn0N/zNCjnb1Qf/brEpy_uEL?track=no'
    elif qualifiedProgram == "Super Annuated Heavy D":
        decodedJSON['id'] = 'OMoWyUyFX'
        decodedJSON['url'] = 'https://letter.cloud.hq.trunorthdigital.com/archive/9ftzvbrep/OMoWyUyFX/QW20RyNMD?track=no'
    elif (qualifiedProgram == "Super Annuated Medium Duty") and (retailerID == "XX101"):
        decodedJSON['id'] = 'rooyW4SNp'
        decodedJSON['url'] = 'https://letter.cloud.hq.trunorthdigital.com/archive/50pj7zwIw/rooyW4SNp/AeMDaRpRy?track=no'
    elif qualifiedProgram == "Super Annuated Medium Duty":
        decodedJSON['id'] = 'bP0zbZpNy'
        decodedJSON['url'] = 'https://letter.cloud.hq.trunorthdigital.com/archive/UTIUJ4wSF/bP0zbZpNy/0xakAT4Kw?track=no'
    elif (qualifiedProgram == "TrüIron") and (retailerID == "XX101"):
        decodedJSON['id'] = 'CpOunJcBF'
        decodedJSON['url'] = 'https://letter.cloud.hq.trunorthdigital.com/archive/kKyxldqfY/CpOunJcBF/d_S0I2IXt?track=no'
    elif qualifiedProgram == "TrüIron":
        decodedJSON['id'] = '1yq4L7IRh'
        decodedJSON['url'] = 'https://letter.cloud.hq.trunorthdigital.com/archive/rlRknT51V/1yq4L7IRh/7Hp2rAp0l?track=no'
    else:
        print('No Valid Qualified Program Selected')

    return decodedJSON

def shorten_url(kwargs):
    #Define Payload to send to url Shortening Service
    urldata = {
    "url" : kwargs['url'],
    "keyword" : kwargs['keyword']
    }

    write_url = 'https://us-central1-repair-facilities-information.cloudfunctions.net/url-shortner'
    payload = urldata
    headers = {'Content-Type': 'application/json'}
    resolved = requests.post(write_url, data=json.dumps(payload), headers=headers)
    print(resolved)
    newUrlData = resolved.json()
    #Get Shortened URL
    shortUrl = newUrlData['url']
    print(shortUrl)
    return shortUrl


def post_subscription(kwargs):

    data = kwargs

    dealID = data['objectId']

    prefilledURLData = prefill_ia_form(dealID)

    inspectionURL = prefilledURLData['url']

    #Pull Full Deal Data from Hubspot
    dealData = get_deal_data(dealID)

    contactAssociationID = dealData['primaryContactID']
    #Get Qualified Program from Deal
    qualifiedProgram = dealData['qualifiedProgram']

    #Get Reffering Dealer ID
    try:
        retailerID = dealData['referring_dealer']
    except:
        print('Unable to get reffering dealer ID')

    contactData = get_contact_data(contactAssociationID)

    #Build JSON data for Mailtrain
    subscriptionData = {}

    #Pull Email Variable for Mailtrain
    contactEmail = contactData['email']
    #Set Email in Mailtrain Subscription Data
    subscriptionData['EMAIL'] = contactEmail

    #Pull Firstname Variable for Mailtrain
    contactFirstName = contactData['firstname']
    subscriptionData['FIRST_NAME'] = contactFirstName

    #Try and Pull Lastname variable for Mailtrain
    try:
        contactLastName = contactData['lastname']
        subscriptionData['LAST_NAME'] = contactLastName
    except:
        print('Last Name not available on contact record')

    keyword = random.getrandbits(64)

    #Define Payload to send to url Shortening Service
    if inspectionURL != "https://apply.mytrunorth.com/#customer-signature/inspection/?view_1638_vars=%7B%22field_1905%22%3A%20%5B%225dd596fe1fa6cd001567a1d6%22%5D%7D":
        urldata = {"url" : inspectionURL,"keyword" : keyword}
        shortenedURL = shorten_url(urldata)
    else:
        shortenedURL = inspectionURL

    subscriptionData['MERGE_INSPECTION_LINK'] = shortenedURL

    #Force Mailtrain Subscription
    subscriptionData['FORCE_SUBSCRIBE'] = 'yes'

    #Configure POST Request to mailtrain
    try:
        #Set mailing list ID
        mailingList = decode_mailtrain_lists(qualifiedProgram, retailerID)
        listID = mailingList['id']
        tmpEmail = mailingList['url']
        print(listID)
        print(tmpEmail)

        if "MERGE_INSPECTION_LINK" in subscriptionData:
            mailtrainurl = 'https://letter.cloud.hq.trunorthdigital.com/api/subscribe/' + listID + '?access_token=390fb26aee7ea2c9615f03b58eb23e37bcabd461'
            print(mailtrainurl)
            resolved = requests.post(mailtrainurl, data=subscriptionData)
            print(resolved)
            print(resolved.json())

            notificationData = {"dealID" : dealID, "emailID" : listID, "retailerAR" : retailerID, "emailURL" : tmpEmail}
            functionURL = 'https://us-central1-repair-facilities-information.cloudfunctions.net/notify-pricing-and-inspection-email-sent'
            payload = notificationData
            headers = {'Content-Type' : 'application/json'}
            resolved = requests.post(functionURL, data=json.dumps(payload), headers=headers)
            print(resolved)
        else:
            notificationData = {"dealID" : dealID}
            functionURL = 'https://us-central1-repair-facilities-information.cloudfunctions.net/notify-pricing-and-inspection-not-sent'
            payload = notificationData
            headers = {'Content-Type' : 'application/json'}
            resolved = requests.post(functionURL, data=json.dumps(payload), headers=headers)
            print(resolved)
            print('Email not sent - Short URL not created')
    except:
        print('Unable to configure Mailtrain POST Request')


#Start Thread
def get_data(request):
    """Responds to any HTTP request.
    Args:
        request (flask.Request): HTTP request object.
    Returns:
        The response text or any set of values that can be turned into a
        Response object using
        `make_response <http://flask.pocoo.org/docs/1.0/api/#flask.Flask.make_response>`.
    """
    #Get Request Data
    data = request.get_json()

    # Trigger Actual Thread in Background
    t = threading.Thread(target=post_subscription, args=(data,))
    t.start()

    # Return 200 so Hubspot is happy
    return '', 200
