import json
import requests
import time

#

#Define Hubspot API Key
HUBSPOT_API_KEY = 'ae30fd45-a516-492a-b281-e91c977a690c'

def get_contact(email):
    #Define Email
    emailAddress = email
    print(emailAddress)
    #Define API URL
    url = 'https://api.hubapi.com/crm/v3/objects/contacts/search?hapikey=' + HUBSPOT_API_KEY
    #Define Search Filter Data
    data = {"filterGroups":[{"filters":[{"propertyName":"email","operator":"EQ","value": emailAddress }]}]}
    #Define Request Header Data
    headers = {'Content-Type' : 'application/json'}
    #Make Request
    resolved = requests.post(url,data=json.dumps(data),headers=headers)
    #Store Returned Data
    data = resolved.json()
    print(data)
    #Get Contact Record
    contactRecord = data['results'][0]
    #Get Contact ID
    contactID = contactRecord['id']
    #Return Contact ID
    return contactID

def get_associated_deals(contactID):
    #Define API URL
    url = 'https://api.hubapi.com/crm/v3/objects/contacts/' + contactID + '/associations/deals?paginateAssociations=false&limit=500&hapikey=' + HUBSPOT_API_KEY
    resolved = requests.get(url)
    #Store Returned Data
    returnedData = resolved.json()
    #Store Results
    results = returnedData['results']
    #Return Full Deal Results
    return results

def get_deal_record_status(dealID):
    #Define API URL
    url = 'https://us-central1-repair-facilities-information.cloudfunctions.net/get-deal-data-with-dealID'
    #Define Data
    data = {"dealID" : dealID}
    #Define Request Header data
    headers = {'Content-Type' : 'application/json'}
    #Make Request
    resolved = requests.post(url, data=json.dumps(data), headers=headers)
    #Store Returned Data
    dealData = resolved.json()
    #Get Deal Pipeline Stage
    dealStage = dealData['dealstage']
    #If Dealstage is "Inspection Auth Sent" return DealID, else return Error
    if (dealStage == "appointmentscheduled") or (dealStage == "2532225"):
        return "sent"
    else:
        return "error"



def create_timeline_activity(kwargs):
    #Configure API URL
    url = 'https://us-central1-repair-facilities-information.cloudfunctions.net/notify-mailgun-delivered-ia-email'
    #Set Request Headers
    headers = {'Content-Type' : 'application/json'}
    #Make Request
    resolved = requests.post(url, data=json.dumps(kwargs), headers=headers)
    print(resolved)

def parse_delivery(kwargs):
    #Store Data from Mailgun
    mailgunData = kwargs

    #Get Email Address
    emailAddress = mailgunData['event-data']['envelope']['targets']
    #Get Sending Domain
    sendingAddress = mailgunData['event-data']['envelope']['sender']
    #Get Delivery Status
    status = mailgunData['event-data']['event']

    #Determine if we return the delivery status
    if status == "delivered":
        return emailAddress

def read_data(request):
    data = request.get_json()

    #Send Webhook Data to Parsing Service
    emailAddress = parse_delivery(data)

    #Get Contact Matching Email Address
    contactID = get_contact(emailAddress)

    #Get Associated Deals
    associatedDeals = get_associated_deals(contactID)

    #Store Final Deals
    finalDeals = []

    #Get Deals That are in deal stage "Inspection Auth form Sent"
    for deal in associatedDeals:
        #Store DealID
        dealID = deal['id']
        #Get Deal Stage Status
        status = get_deal_record_status(dealID)
        #Check if status is correct deal stage
        if status == "sent":
            #Append DealID to finalDeal list
            finalDeals.append(dealID)
        else:
            print('Deal Stage not Correct')

    #Send Deals to Deal Activity Notifier
    for deal in finalDeals:
        #Build Data for Notifier
        notifierData = {
        "dealID" : deal,
        "emailaddress" : emailAddress,
        "sender" : "sales@mytrunorth.com",
        "deliverystatus" : "delivered",
        "subject" : data['event-data']['message']['headers']['subject']
        }
        create_timeline_activity(notifierData)
