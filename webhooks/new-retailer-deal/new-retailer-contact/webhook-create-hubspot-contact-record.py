from flask import escape
from requests import get
import requests
import json
import time
import threading

#define hubspot api key
HUBSPOT_API_KEY = 'ae30fd45-a516-492a-b281-e91c977a690c'

#Create Association between Contact Record and Company Record
def create_record_association(kwargs):
    try:
        associationData = kwargs
        write_url = 'https://us-central1-repair-facilities-information.cloudfunctions.net/hubspot-associate-contact-company'
        print(write_url)
        payload = associationData
        headers = {'Content-Type': 'application/json'}
        requests.post(write_url, data=json.dumps(payload), headers=headers)
    except:
        print('Unable to create association')


#Update Contact Record with HubSpot Contact Record ID
def update_knack_contact_record(kwargs):
    try:
        contactObjID = kwargs['contactObjID']
        print('ContactObjID is :' + contactObjID)
        contactID = kwargs['contactID']
        contactData = {"field_1825" : contactID, "field_1825_raw" : contactID}
        print(contactData)
    except:
        print('Unable to Assemble Data for Knack Record Update')

    #Push payload to knack(contactID to Contact)
    write_url = 'https://api.knack.com/v1/objects/object_48/records/' + contactObjID
    payload = contactData
    headers = {'X-Knack-Application-Id': '57153186de75409353ce581b', 'X-Knack-REST-API-Key': '027cf2d0-05a3-11e6-ade0-cfff8aa34b2b', 'Content-Type': 'application/json'}
    resolved = requests.put(write_url, data=json.dumps(payload), headers=headers)
    print(resolved)
    update = resolved.json()
    return update

#Get Retailer Data via Knack Retailer Record ID
def lookup_retailer(recordID):
    try:
        #wait before looking up retailer
        time.sleep(1.6)
        retailerData = {"retailerObjID" : recordID}
        write_url = 'https://us-central1-repair-facilities-information.cloudfunctions.net/get-company-with-retailerid'
        print(write_url)
        payload = retailerData
        headers = {'Content-Type': 'application/json'}
        resolved = requests.post(write_url, data=json.dumps(payload), headers=headers)
        print(resolved)
        company = resolved.json()
        companyID = company['companyID']
        try:
            ownerID = company['ownerID']
            print("owner Is: " +ownerID)
        except:
            ownerID = None
            print('No Owner ID')
        returnData = {'companyID' : companyID, "ownerID" : ownerID}
        return returnData
    except:
        print('No CompanyID Available')
        returnData = {'error' : 'No CompanyID'}
        return returnData


def set_contact_data(request):

    data = request.get_json()

    contactObjID = data['properties']['knack_contact_record_id']
    retailerObjID = data['properties']['knack_retailer_record_id']

    #Get ContactID
    try:
        contactID = data['contactID']
    except:
        contactID = None
        print('No ContactID in request')
    #Check if ContactID Exists already(Contact Needs to Be Updated with data from SOT)
    if contactID != None:
        properties = data['properties']
        payloadData = {"properties" : properties}
        #Wait .85 Seconds before Calling Hubspot
        time.sleep(.85)
        #Patch Data to Hubspot
        write_url = 'https://api.hubapi.com/crm/v3/objects/contacts/'+ str(contactID) + '?hapikey=' + HUBSPOT_API_KEY
        payload = payloadData
        headers = {'Content-Type': 'application/json'}
        resolved = requests.patch(write_url, data=json.dumps(payload), headers=headers)
        print('Pushed Contact Update to Hubspot')
        return json.dumps({"contactID" : contactID}), 200, {'ContentType' : 'application/json'}
    #Create contact if none exist
    elif contactID == None:
        #Wait before looking up retailer data
        time.sleep(1.3)
        #Get Retailer Data
        retailerData = lookup_retailer(retailerObjID)
        #Set Company ID
        companyID = retailerData['companyID']
        print('CompanyID is: ' + companyID)
        #Try and set Owner ID if available
        try:
            retailerOwnerID = retailerData['ownerID']
        except:
            print('No Owner ID')
        #Try and set Owner ID on Contact
        try:
            data['properties']['hubspot_owner_id'] = retailerOwnerID
        except:
            print('Cannot Set Blank Owner on Contact Record')

        #Wait before posting Data to hubspot
        time.sleep(1.7)
        #Post Data to Hubspot
        write_url = 'https://api.hubapi.com/crm/v3/objects/contacts?hapikey=' + HUBSPOT_API_KEY
        payload = data
        headers = {'Content-Type': 'application/json'}
        postData = requests.post(write_url, data=json.dumps(payload), headers=headers)
        contactData = postData.json()
        #wait before trying to push update to contact
        time.sleep(1.8)
        try:
            contactID = contactData['id']
        except:
            contactID = contactData['message']
            contactID = contactID.split(": ", 1)
            contactID = contactID[1]
            print(contactID)
            write_url = 'https://api.hubapi.com/crm/v3/objects/contacts/'+ str(contactID) + '?hapikey=' + HUBSPOT_API_KEY
            payload = data
            headers = {'Content-Type': 'application/json'}
            resolved = requests.patch(write_url, data=json.dumps(payload), headers=headers)
            print('Updated Contact without Creating Duplicate')
        finally:
            #Wail Before Calling Hubspot Association API
            time.sleep(2)
            #Create association Data
            associationData = {"contactID" : contactID, "companyID" : companyID}
            #Call Association API
            create_record_association(associationData)
            #Create Knack Contact Data
            knackContactData = {"contactObjID" : contactObjID, "contactID" : contactID}
            #Wait before calling Knack
            time.sleep(2.2)
            update = update_knack_contact_record(knackContactData)
            print(update)
            print('Pushed Contact to Hubspot')
            return json.dumps({"contactID" : contactID}), 200, {'ContentType' : 'application/json'}
