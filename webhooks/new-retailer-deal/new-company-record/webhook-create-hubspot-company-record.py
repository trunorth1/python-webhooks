from flask import escape
from requests import get
import requests
import json
import time
import threading

#define hubspot api key
HUBSPOT_API_KEY = 'ae30fd45-a516-492a-b281-e91c977a690c'

def update_knack_retailer_record(kwargs):
    try:
        retailerObjID = kwargs['retailerObjID']
    except:
        print('No Retailer Obj ID')

    try:
        companyID = kwargs['companyID']
        companyData = {'field_1823' : companyID, 'field_1823_raw' : companyID}
    except:
        print('No ComapnyID')

    #Push payload to knack(CompanyID to Retailer)
    write_url = 'https://api.knack.com/v1/objects/object_9/records/' + retailerObjID
    payload = companyData
    headers = {'X-Knack-Application-Id': '57153186de75409353ce581b', 'X-Knack-REST-API-Key': '027cf2d0-05a3-11e6-ade0-cfff8aa34b2b', 'Content-Type': 'application/json'}
    resolved = requests.put(write_url, data=json.dumps(payload), headers=headers)


def set_retailer_data(request):

    data = request.get_json()

    retailerObjID = data['properties']['knack_retailer_record_id']
    #LookUp Retailer ID
    #Wait for .75 Milliseconds
    time.sleep(.750)

    #Get CompanyID
    try:
        companyID = data['companyID']
    except:
        companyID = None
        print('No CompanyID')
    if companyID != None:
        properties = data['properties']
        payloadData = {"properties" : properties}
        #Patch Data to Hubspot
        write_url = 'https://api.hubapi.com/crm/v3/objects/companies/'+ str(companyID) + '?hapikey=' + HUBSPOT_API_KEY
        payload = payloadData
        headers = {'Content-Type': 'application/json'}
        resolved = requests.patch(write_url, data=json.dumps(payload), headers=headers)
        print('Pushed Company Update to Hubspot')
        retailerData = {'retailerObjID' : retailerObjID, 'companyID' : companyID}
        #Return Retailer Data
        return json.dumps(retailerData), 200, {'Content-Type':'application/json'}
    elif companyID == None:
        #Patch Data to Hubspot
        write_url = 'https://api.hubapi.com/crm/v3/objects/companies?hapikey=' + HUBSPOT_API_KEY
        payload = data
        headers = {'Content-Type': 'application/json'}
        postData = requests.post(write_url, data=json.dumps(payload), headers=headers)
        companyData = postData.json()
        companyID = companyData['id']
        retailerData = {'retailerObjID' : retailerObjID, 'companyID' : companyID}
        #Wait before calling Knack
        time.sleep(1)
        update_knack_retailer_record(retailerData)
        print('Pushed Company Creation to Hubspot')
        #Return Retailer Data
        return json.dumps(retailerData), 200, {'Content-Type':'application/json'}
