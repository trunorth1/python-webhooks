import requests
import json
import time

#define hubspot api key
HUBSPOT_API_KEY = 'ae30fd45-a516-492a-b281-e91c977a690c'

#Get Knack Retailer Record
def get_retailer_record(retailerObjID):
    #Wait for 10 seconds
    time.sleep(10)
    #Define API URL
    url = 'https://api.knack.com/v1/objects/object_9/records/' + retailerObjID
    #Define Headers
    headers = {'X-Knack-Application-Id': '57153186de75409353ce581b', 'X-Knack-REST-API-Key': '027cf2d0-05a3-11e6-ade0-cfff8aa34b2b'}
    #Define Request
    resolved = requests.get(url, headers=headers)
    #Get Record
    retailerRecord = resolved.json()
    #Return Retailer Record
    return retailerRecord


#Create Hubspot Company Record
def create_company(kwargs):
    #Define API URL
    url = 'https://us-central1-repair-facilities-information.cloudfunctions.net/create-new-hs-retailer-record'
    #Define Payload Data
    payload = {'properties' : kwargs }
    #Define Request Headers
    headers = {'Content-Type' : 'application/json'}
    #Define Request
    resolved = requests.post(url, data=json.dumps(payload), headers=headers)
    #Store Request Response JSON
    hubspotRecord = resolved.json()
    #log to console
    print(resolved.json())
    #Get Company ID
    companyID = hubspotRecord['companyID']
    #Return CompanyID
    return companyID

#Create Hubspot Contact Record
def create_hs_contact(kwargs):
    #Define API URL
    url = 'https://us-central1-repair-facilities-information.cloudfunctions.net/create-new-retailer-contact'
    #Define Payload Data
    payload = {'properties' : kwargs }
    #Define Request headers
    headers = {'Content-Type' : 'application/json'}
    #Define Request
    resolved = requests.post(url, data=json.dumps(payload), headers=headers)
    #Store Request JSON
    hubspotRecord = resolved.json()
    #Log to Console
    print(resolved)
    print(resolved.json())

def create_knack_contact(kwargs):
    #Define API URL
    url = 'https://api.knack.com/v1/objects/object_48/records'
    #Define Payload
    payload = kwargs
    #Define Request Headers
    headers = {'X-Knack-Application-Id': '57153186de75409353ce581b', 'X-Knack-REST-API-Key': '027cf2d0-05a3-11e6-ade0-cfff8aa34b2b', 'Content-Type': 'application/json'}
    #Define Request
    resolved = requests.post(url, data=json.dumps(payload), headers=headers)
    #Store Request JSON
    contactRecord = resolved.json()
    #Store Contact Record ID
    contactObjID = contactRecord['id']
    #Return Contact Record ID
    return contactObjID

#Create Hubspot Deal Record
def create_deal(kwargs):
    #Define API URL
    url = 'https://api.hubapi.com/crm/v3/objects/deals?hapikey=' + HUBSPOT_API_KEY
    #Define Payload
    payload = {'properties' : kwargs}
    #Define Headers
    headers = {'Content-Type' : 'application/json'}
    #Define Request
    resolved = requests.post(url, data=json.dumps(payload),headers=headers)
    #Get Deal
    dealData = resolved.json()
    #Deal ID
    dealID = dealData['id']
    #Return DealID
    return dealID


#Create Company to Deal Association
def create_deal_association(dealID, contactID, companyID):
    #Create Association Deal to Company
    url = 'https://api.hubapi.com/crm/v3/objects/deals/' + str(dealID) + '/associations/company/' + str(companyID) + '/deal_to_company?paginateAssociations=false&hapikey=' + HUBSPOT_API_KEY
    #Push Association
    resolved = requests.put(url)
    #Log to console
    print(resolved)

    #Create Association Deal to Contact
    url = 'https://api.hubapi.com/crm/v3/objects/deals/'+str(dealID)+'/associations/contact/'+str(contactID)+'/deal_to_contact?paginateAssociations=false&hapikey=' + HUBSPOT_API_KEY
    #Push Association
    resolved = requests.put(url)
    #Log to console
    print(resolved)

#Read Owners based on Knack Record ID
def read_hs_owners(kwargs):
    #Set List Identifier to None
    list_identifier = None
    userID = None

    #Set Contact List ID based on Knack Regional Manager Record ID
    if recordID == '59ea04395275574b75450917':
        print('Lance Eskridge')
        list_identifier = '285'
        userID = '50804722'
    elif recordID == '5af9a7038b8af72a442f116e':
        print('Brie Hunt')
        list_identifier = '283'
        userID = '50804617'
    elif recordID == '59e778e563bc9f4b71b4a964':
        print('Jennifer Cartier')
        list_identifier = '280'
        userID = '50804599'
    elif recordID == '5ee396858ccfb90015fa01b7':
        print('Michelle Wilson')
        list_identifier = '284'
        userID = '50804712'
    elif recordID == '59ea0399ca9ac74b473e95d3':
        print('Rick Van Hove')
        list_identifier = '281'
        userID = '50804710'
    elif recordID == '59ea03c4ace9024b75217dc5':
        print('Taylor Hobson')
        list_identifier = '286'
        userID = '50355205'
    elif recordID == '5a8c8a5066694a72da3a0676':
        print('Willie Boyle')
        list_identifier = '282'
        userID = '50804605'
    elif recordID == '5a031333fbc8744e8d5da8c3':
        print('Lindsey Grammel')
        list_identifier = ''
        userID = '39438000'
    elif recordID == '5bc4c877435dd60a66196dd0':
        print ('Eric Fruithandler')
        list_identifier = ''
        userID = '50804612'
    else:
        print('No Valid Regional Manager Selected')

    regionalManager = {
    "listIdentifier" : list_identifier,
    "userID" : userID
    }

    return regionalManager

#Parse Request Data
def parse_retailer_data(kwargs):
    #Define Return JSON
    retailerData = {}

    #Get Retailer Name
    try:
        retailerData['name'] = kwargs['field_149']
    except:
        print('No Retailer Name Available')
        retailerData['name'] = 'Not Available'

    #Get Retailer Phone Number
    try:
        retailerData['phone'] = kwargs['field_120_raw']['formatted']
    except:
        print('Phone Number Not Available')
        retailerData['phone'] = 'Not Available'

    #Get City
    try:
        retailerData['city'] = kwargs['field_121_raw']['city']
    except:
        print('City not Available')
        retailerData['city'] = 'Not Available'

    #Get Zip Code
    try:
        retailerData['zipcode'] = kwargs['field_121_raw']['zip']
    except:
        print('Zip Code not Available')
        retailerData['zipcode'] = 'Not Available'

    #Get State
    try:
        retailerData['state'] = kwargs['field_121_raw']['state']
    except:
        print('State not Available')
        retailerData['state'] = 'Not Available'

    #Get Street
    try:
        retailerData['street'] = kwargs['field_121_raw']['street']
    except:
        print('Street not Available')
        retailerData['street'] = 'Not Available'

    #Get Email Address
    try:
        retailerData['email'] = kwargs['field_553_raw']['email']
    except:
        print('Email Address Not Available')
        retailerData['email'] = 'Not Available'

    #Get Website URL
    try:
        email = kwargs['field_553_raw']['email']
        url = email.split('@',1)
        url = url[1]
        retailerData['website'] = url
        print(url)
    except:
        print('Website Not Available')
        retailerData['Website'] = 'Not Available'

    #Get Retailer Owner
    try:
        tmpOwner = kwargs['field_661_raw'][0]['id']
        #Get RegionalManagerData
        regionalManagerData = read_hs_owners(tmpOwner)
        #userID
        userID = regionalManagerData['userID']
        #log to console
        print(userID)
        #SetOwneriD
        retailerData['hubspot_owner_id'] = userID
    except:
        print('Owner ID not Available')
        retailerData['hubspot_owner_id'] = 'Not Available'

    #Get Terrory
    try:
        retailerData['territory'] = kwargs['field_1460_raw'][0]['identifier']
    except:
        print('T Unavilable')

    return retailerData


#Parse Contact Data
def parse_contact_data(data, retailerData):
    #Define Return JSON
    contactData = {}

    #Get Name
    try:
        #Get Full Name
        fullName = data['name']
        print(fullName)
        #Split Full Name
        name_arr = fullName.split(" ", 1)
        #Get firstName
        firstName = name_arr[0]
        contactData['firstname'] = firstName
        #Get Last Name
        lastName = name_arr[1] if len(name_arr) > 1 else ""
        contactData['lastname'] = lastName
    except:
        print('Name Not Available')
        contactData['firstname'] = 'NA'
        contactData['lastname'] = 'NA'

    #Get Email
    try:
        contactData['email'] = data['email']
    except:
        print('Email Not Available')
        contactData['email'] = 'Not Available'

    #Get Phone Number
    try:
        contactData['phone'] = data['phone']
    except:
        print('Phone Number not Available')
        contactData['phone'] = 'NA'

    #Get City
    try:
        contactData['city'] = retailerData['field_121_raw']['city']
    except:
        print('City not Available')
        contactData['city'] = 'Not Available'

    #Get Zip Code
    try:
        contactData['zip'] = retailerData['field_121_raw']['zip']
    except:
        print('Zip Code not Available')
        contactData['zip'] = 'Not Available'

    #Get State
    try:
        contactData['state'] = retailerData['field_121_raw']['state']
    except:
        print('State not Available')
        contactData['state'] = 'Not Available'

    #Get Street
    try:
        contactData['address'] = retailerData['field_121_raw']['street']
    except:
        print('Street not Available')
        contactData['address'] = 'Not Available'

    #Get HS Owner
    try:
        tmpOwner = retailerData['field_661_raw'][0]['id']
        #Get RegionalManagerData
        regionalManagerData = read_hs_owners(tmpOwner)
        #userID
        userID = regionalManagerData['userID']
        #log to console
        print(userID)
        #SetOwneriD
        contactData['hubspot_owner_id'] = userID
    except:
        print('Owner ID not Available')
        contactData['hubspot_owner_id'] = 'Not Available'

    #Set Knack Retailer Obj ID
    try:
        contactData['knack_retailer_record_id'] = retailerData['id']
    except:
        print('RetailerObjID Not Available')

    return contactData

def parse_contact_for_knack(kwargs):
    #define return JSON
    knackContact = {}

    #Set Contact Name
    try:
        firstName = kwargs['firstname']
        lastName = kwargs['lastname']
        knackContact['field_876'] = str(firstName) + " " + str(lastName)
    except:
        print('Name Not Available')

    #Set Contact Type
    knackContact['field_868'] = 'Retailer'

    #Set Contact Email
    try:
        knackContact['field_869'] = {'email' : kwargs['email'], 'label' : kwargs['email']}
    except:
        print('No Email Available')

    #Set Contact Phone Number
    try:
        knackContact['field_870'] = kwargs['phone']
    except:
        print('Phone Number Not Available')

    #Set Address
    address = {}
    try:
        address['street'] = kwargs['address']
    except:
        print('Street Not Available')
    try:
        address['zip'] = kwargs['zip']
    except:
        print('zip not available')
    try:
        address['city'] = kwargs['city']
    except:
        print('city not available')
    try:
        address['state'] = kwargs['state']
    except:
        print('state not available')

    #Set address in Knack JSON
    knackContact['field_871'] = address

    #Set Associated Retailers
    try:
        knackContact['field_878'] = [kwargs['knack_retailer_record_id']]
    except:
        print('RetailerOBJID Not Available')

    #Return Knack Contact Data
    return knackContact


def read_data(request):
    #Store Request Data
    data = request.get_json()

    #Get RetailerObjID
    retailerObjID = data['retailerObj']['id']
    print(retailerObjID)

    #Get Retailer Record
    retailer = get_retailer_record(retailerObjID)
    print(retailer)

    #Parse Retailer Data
    retailerData = parse_retailer_data(retailer)
    print(retailerData)

    #Parse Contact Data
    hsContactData = parse_contact_data(data, retailer)
    print(hsContactData)

    #Build Contact for Knack
    knackContactData = parse_contact_for_knack(hsContactData)
    print(knackContactData)

    #Create Contact Record in Knack
    contactObjID = create_knack_contact(knackContactData)

    #Add Knack Contact Record ID to Hubspot Contact
    hsContactData['knack_contact_record_id'] = contactObjID

    #Create Hubspot Company Record
    companyID = create_company(retailerData)

    #Wait 30 seconds before creating contact record
    time.sleep(30)

    #Create Hubspot Contact Record
    hsContactRecord = create_hs_contact(hsContactData)

    #Try to get ContactID
    try:
        contactID = hsContactRecord['contactID']
    except:
        contactID = None

    #Build Deal Properties
    dealJSON = {}
    dealJSON['dealname'] = retailerData['name']
    dealJSON['dealstage'] = '7567893'
    dealJSON['pipeline'] = '7567041'

    #Create Deal
    dealID = create_deal(dealJSON)

    #Associate Deal Objects
    create_deal_association(dealID, contactID, companyID)

    #Return
    return json.dumps({'No Error':'No Error'}), 200, {'Content-Type':'application/json'}
