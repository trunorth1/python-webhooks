import requests
import json
import time

#define hubspot api key
HUBSPOT_API_KEY = 'ae30fd45-a516-492a-b281-e91c977a690c'

#Shorten Signed Agreement Link
def shorten_signed_agreement(url):
    #Define API URL
    url = 'https://us-central1-repair-facilities-information.cloudfunctions.net/url-shortener-kutt-platform'
    #Define Request Headers
    headers = {'Content-Type' : 'application/json'}
    #Define Payload data
    payload = {'url' : url}
    #Create Request
    resolved = requests.post(url, data=json.dumps(payload),headers=headers)
    #Store response as JSON
    response = resolved.json()
    #Get shortened URL from response
    shortURL = response['url']
    #Return shortenedURL
    return shortURL

#Get Company with RetailerObjID
def get_company_record(retailerObjID):
    #Define API URL
    url = 'https://us-central1-repair-facilities-information.cloudfunctions.net/get-company-with-retailerid'
    #Define Request Headers
    headers = {'Content-Type' : 'application/json'}
    #Define Payload Data
    payload = {'retailerObjID' : retailerObjID}
    #Create Request
    resolved = requests.post(url, data=json.dumps(payload), headers=headers)
    #Store response
    response = resolved.json()
    #Get CompanyID from Response
    companyID = response['companyID']
    #Return CompanyID
    return companyID

#Get Deal In Retailer Sign Up pipeline Connected to Company
def get_deal(companyID):
    ##Get all Deals associated to companyID
    #Define API URL
    url = 'https://api.hubapi.com/crm/v3/objects/companies/' + companyID + '/associations/deal?paginateAssociations=false&limit=500&hapikey=' + HUBSPOT_API_KEY
    #Create Request
    resolved = requests.get(url)
    #Store Response
    response = resolved.json()
    #Get Deals from response
    deals = response['results']

    ##Loop Through Associated Deals and Only return deal which is in Retailer Signup Pipeline
    for record in deals:
        #Store dealID
        dealID = record['id']
        ##Get Deal record
        #Define API URL
        url = 'https://api.hubapi.com/crm/v3/objects/deals/'+str(dealID)+'?properties=pipeline&paginateAssociations=false&archived=false&hapikey='+HUBSPOT_API_KEY
        #Create Request
        resolved = requests.get(url)
        #Store Response
        response = resolved.json()
        #Check Deal Pipeline
        if response['properties']['pipeline'] == '7567041':
            finalDealID = dealID
            return finalDealID
        else:
            print('Pipeline Incorrect')

#Push Activity Notifier to Company Record
def update_company_record(companyID, agreementLink):
    #Define API URL
    url = 'https://us-central1-repair-facilities-information.cloudfunctions.net/notify-companyrecord-retailer-signed-agreement'
    #Define Request Headers
    headers = {'Content-Type' : 'application/json'}
    #Define Payload
    payload = {'companyID' : companyID, 'url' : agreementLink}
    #Create Request
    resolved = requests.post(url, data=json.dumps(payload, headers=headers)
    print(resolved)
    return resolved

#Push Activity Notifier to Deal Record
def update_deal_record(dealID):
    #Define API URL
    url = 'https://us-central1-repair-facilities-information.cloudfunctions.net/notify-dealrecord-retailer-signed-agreement'
    #Define Request Headers
    headers = {'Content-Type' : 'application/json'}
    #Define Payload
    payload = {'dealID' : dealID}
    #Create Request
    resolved = requests.post(url, data=json.dumps(payload, headers=headers)
    print(resolved)
    return resolved


#Read Request Data
def read_data(request):
    #Read Data from Request
    data = request.get_json()

    #Get RetailerObjID
    retailerObjID = data['retailerObjID']

    #Get link to signed agreement
    signedAgreement = data['signedAgreement']

    #Get Company Record ID with retailerObjID
    companyID = get_company_record(retailerObjID)

    #Get DealID to Associate to
    dealID = get_deal(companyID)

    #Shorten Signed Agreement Link
    try:
        shortenedURL = shorten_signed_agreement(signedAgreement)
    except:
        print('Unable to Shorten URL')
        shortenedURL = signedAgreement

    #Update Company Record with the Signed Agreement Link and Agreement Status
    update_company_record(companyID, shortenedURL)

    #Update Deal Activity Timeline with Signed Data/Status
    update_deal_record(dealID)
