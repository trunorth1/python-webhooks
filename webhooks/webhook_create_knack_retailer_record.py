import requests
import json
import time
import threading

#define hubspot api key

#Read Owners based on Hubspot Owner ID
def read_hs_owners(recordID):
    #Set List Identifier to None
    list_identifier = None
    userID = None
    #Set Contact List ID based on Knack Regional Manager Record ID
    if recordID == '50804722':
        print('Lance Eskridge')
        list_identifier = '285'
        userID = '59ea04395275574b75450917'
    elif recordID == '50804617':
        print('Brie Hunt')
        list_identifier = '283'
        userID = '5af9a7038b8af72a442f116e'
    elif recordID == '50804599':
        print('Jennifer Cartier')
        list_identifier = '280'
        userID = '59e778e563bc9f4b71b4a964'
    elif recordID == '50804712':
        print('Michelle Wilson')
        list_identifier = '284'
        userID = '5ee396858ccfb90015fa01b7'
    elif recordID == '50804710':
        print('Rick Van Hove')
        list_identifier = '281'
        userID = '50804710'
    elif recordID == '50355205':
        print('Taylor Hobson')
        list_identifier = '286'
        userID = '59ea03c4ace9024b75217dc5'
    elif recordID == '50804605':
        print('Willie Boyle')
        list_identifier = '282'
        userID = '5a8c8a5066694a72da3a0676'
    elif recordID == '39438000':
        print('Lindsey Grammel')
        list_identifier = ''
        userID = '5a031333fbc8744e8d5da8c3'
    elif recordID == '50804612':
        print ('Eric Fruithandler')
        list_identifier = ''
        userID = '5bc4c877435dd60a66196dd0'
    else:
        print('No Valid Regional Manager Selected')

    regionalManager = {
    "listIdentifier" : list_identifier,
    "userID" : userID
    }

    return regionalManager

#Get Hubspot Company Record
def get_company_record(companyID):
    #Define API URL
    url = 'https://api.hubapi.com/crm/v3/objects/companies/'+str(companyID)+'?properties=dba%2Cname%2Ccity%2Cstate%2Czip%2Caddress%2Chubspot_owner_id%2Ctax_id%2Cphone&associations=contacts&paginateAssociations=false&archived=false&hapikey=' + HUBSPOT_API_KEY
    #Make Request
    resolved = requests.get(url)
    #Store Response
    response = resolved.json()
    #Return Response
    return response

#Parse Hubspot Company Record
def parse_company(kwargs):

    #Define Return JSON
    retailerData = {}
    #Get Retailer Name
    try:
        retailerData['retailerName'] = kwargs['name']
    except:
        print('No Retailer Name Available')

    #Get Retailer Phone Number
    try:
        retailerData['phone'] = kwargs['phone']
    except:
        print('Phone Number Not Available')

    #Get City
    try:
        retailerData['city'] = kwargs['city']
    except:
        print('City not Available')

    #Get Zip Code
    try:
        retailerData['zip'] = kwargs['zip']
    except:
        print('Zip Code not Available')

    #Get State
    try:
        retailerData['state'] = kwargs['state']
    except:
        print('State not Available')

    #Get Street
    try:
        retailerData['street'] = kwargs['address']
    except:
        print('Street not Available')

    #Get Retailer Owner
    try:
        tmpOwner = kwargs['hubspot_owner_id']
        #Get RegionalManagerData
        regionalManagerData = read_hs_owners(tmpOwner)
        #userID
        userID = regionalManagerData['userID']
        #log to console
        print(userID)
        #SetOwneriD
        retailerData['retailerOwner'] = userID
    except:
        print('Owner ID not Available')

    #Get DBA
    try:
        retailerData['dba'] = kwargs['dba']
    except:
        print('DBA not Available')

    #Get Tax ID
    try:
        retailerData['taxID'] = kwargs['tax_id']
    except:
        print('No Tax ID Available')


    return retailerData

#Create Retailer Record with Ryan's Endpoint
def create_retailer(retailerData):
    #Define API URL
    url = ''
    #Define Request Headers
    headers = {'Content-Type' : 'application/json'}
    #Make Request
    resolved = requests.post(url, data=json.dumps(retailerData), headers=headers)
    #Store response
    response = resolved.json()
    #Get RetailerObjID from Response
    retailerObjID = response['retailerObjID']
    #return retailerObjID
    return retailerObjID


#Write RetailerObjID to Hubspot Company Record
def update_company_record(companyID, retailerObjID):
    pass

#Read Data passed by initial thread
def read_data(data):

    #Read data from initial thread and get objectId
    objectId = data['objectId']

    #Get Company Record with ObjectId
    companyRecordData = get_company_record(objectId)

    #Get Properties from Company Record
    companyProperties = companyRecordData['properties']

    #Parse Out Company Record into Data Knack can Read
    knackRecordData = parse_company(companyProperties)

    #Create Retailer Record in Knack and Return retailerObjID
    retailerObjID = create_retailer(knackRecord)

    #Push Update to Company Record with RetailerObjID
    update_company_record(companyID, retailerObjID)


#Create initial thread to return 200 instantly
def initial_thread(request):
    #Store initial request data
    data = request.get_json()

    #Create secondary thread for target function
    t = threading.Thread(target=read_data, args=(data, )
    #Start secondary thread
    t.start()

    #Return 200 to hubspot
    return '', 200
