import requests
import json
import time

#define hubspot api key
HUBSPOT_API_KEY = 'ae30fd45-a516-492a-b281-e91c977a690c'

#Shorten Provided URL
def shorten_url(url):
    #Define API URL
    url = 'https://us-central1-repair-facilities-information.cloudfunctions.net/url-shortener-kutt-platform'
    #Define Request Headers
    headers = {'Content-Type' : 'application/json'}
    #Define Payload data
    payload = {'url' : url}
    #Create Request
    resolved = requests.post(url, data=json.dumps(payload),headers=headers)
    #Store response as JSON
    response = resolved.json()
    #Get shortened URL from response
    shortURL = response['url']
    #Return shortenedURL
    return shortURL

#Store URL in Hubspot Company Record
def write_to_company(url, companyID):
    pass

#Get retailerObjID from Company Record
def get_company_retailerObjID(companyID):
    #Define API URL
    url = 'https://api.hubapi.com/crm/v3/objects/companies/' + str(companyID) + '?properties=knack_retailer_record_id&paginateAssociations=false&archived=false&hapikey=' + HUBSPOT_API_KEY
    #Create Request
    resolved = requests.get(url)
    #Store response
    response = resolved.json()
    #Get RetailerObjID from response
    retailerObjID = response['properties']['knack_retailer_record_id']
    #Return RetailerObjID
    return retailerObjID

#Pass RetailerObjID to Ryan to create Retailer SignUp Form URL
def create_signup_url(retailerObjID):
    #Return Full Length Link
    pass

#Read Request Data
def read_data(request):
    #Store Request
    data = request.get_json()

    #Get Company ID
    companyID = data['objectId']

    #Get RetailerObjID from Company Record
    retailerObjID = get_company_retailerObjID(companyID)

    #create retailer signup form
    fullLengthURL = create_signup_url(retailerObjID)

    #shorten url
    try:
        shortenedURL = shorten_url(fullLengthURL)
    except:
        print('Unable to shorten URL')
        shortenedURL = fullLengthURL

    #write shortenedURL to Hubspot Company
    response = write_to_company(shortenedURL, companyID)
