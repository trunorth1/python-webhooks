from requests import get
import requests
import json
import time
import threading

#Test File for Deployment

#define hubspot api key
HUBSPOT_API_KEY = 'ae30fd45-a516-492a-b281-e91c977a690c'

#Get Retailer Data via Knack Retailer Record ID
def lookup_retailer(recordID):
    try:
        retailerData = {"companyID" : recordID}
        write_url = 'https://us-central1-repair-facilities-information.cloudfunctions.net/get_company_with_companyID'
        print(write_url)
        payload = retailerData
        headers = {'Content-Type': 'application/json'}
        resolved = requests.post(write_url, data=json.dumps(payload), headers=headers)
        print(resolved)
        company = resolved.json()
        print(company)
        returnData = {}
        try:
            knack_retailer_record_id = company['knack_retailer_record_id']
            if (knack_retailer_record_id != None) and (knack_retailer_record_id != ''):
                returnData['knack_retailer_record_id'] = knack_retailer_record_id
        except:
            print('No Knack Retailer Record ID')
        return returnData
    except:
        print('No CompanyID Available')
        returnData = {'error' : 'No CompanyID'}
        return returnData


#Update Knack Contact Record with HubSpot Contact Record Data
def update_knack_contact_record(contactRecord):
    write_url = 'https://us-central1-repair-facilities-information.cloudfunctions.net/update-knack-contact-record'
    print(write_url)
    payload = contactRecord
    headers = {'Content-Type': 'application/json'}
    resolved = requests.post(write_url, data=json.dumps(payload), headers=headers)

def create_knack_contact_record(contactRecord):
    write_url = 'https://us-central1-repair-facilities-information.cloudfunctions.net/create-knack-contact-record'
    print(write_url)
    payload = contactRecord
    headers = {'Content-Type': 'application/json'}
    resolved = requests.post(write_url, data=json.dumps(payload), headers=headers)
    response = resolved.json()
    contactObjID = response['contactObjID']
    return contactObjID

def get_contact_record(contactID):
    contactData = {'contactID' : contactID}
    write_url = 'https://us-central1-repair-facilities-information.cloudfunctions.net/get_contact_with_contactID'
    print(write_url)
    payload = contactData
    headers = {'Content-Type': 'application/json'}
    resolvedData = requests.post(write_url, data=json.dumps(payload), headers=headers)
    finalData = resolvedData.json()
    return finalData

def update_hubspot_contact_record(contactID, kwargs):
    #Define API URL
    url = 'https://api.hubapi.com/crm/v3/objects/contacts/'+str(contactID)+'?hapikey=' + HUBSPOT_API_KEY
    #Build API Payload
    payload = {"properties" : kwargs}
    #Define Request Headers
    headers = {'Content-Type' : 'application/json'}
    #Make Request
    resolved = requests.patch(url, data=json.dumps(payload), headers=headers)
    #log to console
    print(resolved)
    print(resolved.json())


#Send Notification to Contact Record Activity Timeline that Contact Record has been Updated/Created in TDP
def notify_contact_updated(contactID):
    contactRecord = {"contactID" : contactID}
    write_url = 'https://us-central1-repair-facilities-information.cloudfunctions.net/notify_contact_pushed_to_tdp'
    print(write_url)
    payload = contactRecord
    headers = {'Content-Type': 'application/json'}
    resolved = requests.post(write_url, data=json.dumps(payload), headers=headers)

def parse_record(contactRecord):
    #Build JSON Data for Knack Contact Record Update
    knackRecordUpdate = {}
    recordData = {}
    knackRecordUpdate['recordData'] = recordData

    #Get Contact RecordID
    try:
        knackRecordUpdate['recordID'] = str(contactRecord['knack_contact_record_id'])
    except:
        print('No Contact Record ID Available')
    #Set Contact Full Name in JSON Data
    try:
        knackRecordUpdate['recordData']['field_876'] = contactRecord['firstname'] + " " + contactRecord['lastname']
    except:
        print('Contact has no name on file')
    #Set Phone Number in JSON Data
    try:
        knackRecordUpdate['recordData']['field_870_raw'] = {'formatted' : contactRecord['phone']}
        knackRecordUpdate['recordData']['field_870'] = contactRecord['phone']
    except:
        print('Contact has no phone number on file')
    #Set Email Address in JSON Data
    try:
        knackRecordUpdate['recordData']['field_869_raw'] = {'email' : contactRecord['email']}
        knackRecordUpdate['recordData']['field_869'] = contactRecord['email']
    except:
        print('Contact has no email address on file')
    #Set Hubspot Contact Record ID in JSON Data
    try:
        knackRecordUpdate['recordData']['field_1825'] = contactRecord['contactID']
    except:
        print('No Contact ID on Record')
    #Set Retailer Record ID ONLY if the association has changed on the contact
    try:
        #Current Retailer Record ID on Contact
        retailerObjID = contactRecord['knack_retailer_record_id']
        #Set Contacts Retailer
        knackRecordUpdate['recordData']['field_878_raw'] = [retailerObjID]
        knackRecordUpdate['recordData']['field_878'] = retailerObjID
    except:
        print('Unable to Set Retailer ID')

    #Set Contact Type
    try:
        if contactRecord['contact_type'] == 'primary_retailer_contact':
            knackRecordUpdate['recordData']['field_949'] = 'TruNorth Primary Dealer Contact'
            knackRecordUpdate['recordData']['field_868'] = 'Retailer'
            knackRecordUpdate['recordData']['field_949_raw'] = 'TruNorth Primary Dealer Contact'
            knackRecordUpdate['recordData']['field_868_raw'] = 'Retailer'
        elif contactRecord['contact_type'] == 'retailer_contact':
            knackRecordUpdate['recordData']['field_949'] = 'TruNorth Other Contact'
            knackRecordUpdate['recordData']['field_868'] = 'Retailer'
            knackRecordUpdate['recordData']['field_949_raw'] = 'TruNorth Other Contact'
            knackRecordUpdate['recordData']['field_868_raw'] = 'Retailer'
        elif contactRecord['contact_type'] == 'retailer_accounting_contact':
            knackRecordUpdate['recordData']['field_949'] = 'TruNorth Accounting Contact'
            knackRecordUpdate['recordData']['field_868'] = 'Retailer'
            knackRecordUpdate['recordData']['field_949_raw'] = 'TruNorth Accounting Contact'
            knackRecordUpdate['recordData']['field_868_raw'] = 'Retailer'
        elif contactRecord['contact_type'] == 'warranty_holder':
            knackRecordUpdate['recordData']['field_868'] = 'Warranty Holder'
            knackRecordUpdate['recordData']['field_868_raw'] = 'Warranty Holder'
            knackRecordUpdate['recordData']['field_949_raw'] = ''
            knackRecordUpdate['recordData']['field_949'] = ''
            pass
        else:
            print('Type Not Valid')
    except:
        print('No Valid Type Available')

    return knackRecordUpdate


def start_write_function(data):

    data = data
    #Get ContactID from WebHook Data
    contactID = data['objectId']

    #Get Contact Record Data
    contactRecord = get_contact_record(contactID)

    #Set ContactID in contactRecord
    contactRecord['contactID'] = contactID

    #Define Blank retailerObjID
    retailerObjID = None
    ar = None

    #Get Company Knack Record ID
    try:
        companyID = contactRecord['associatedCompany']
        print(companyID)
        companyData = lookup_retailer(companyID)
        contactRecord['knack_retailer_record_id'] = str(companyData['knack_retailer_record_id'])
        retailerObjID = str(companyData['knack_retailer_record_id'])
        ar = str(companyData['ar'])
    except:
        print('No Company Associated with Contact')


    #Parse Hubspot Contact Record to get into Knack Format
    knackRecordData = parse_record(contactRecord)

    #Decide if Contact is being Updated or Created
    if 'recordID' in knackRecordData:
        #Update Knack Contact Record
        update_knack_contact_record(knackRecordData)
        #Define Update Data
        updateData = {"sync_contact_update" : "false", "knack_retailer_record_id" : retailerObjID, "ar_" : ar}
        #Update Hubspot Contact Record
        update_hubspot_contact_record(contactID, updateData)
    else:
        #Create New Knack Contact Record
        contactObjID = create_knack_contact_record(knackRecordData)
        print(contactObjID)
        #Define Update Data
        updateData = {"sync_contact_update" : "false", "knack_contact_record_id" : str(contactObjID),"knack_retailer_record_id" : retailerObjID, "ar_" : ar}
        #Update Hubspot Contact Record
        update_hubspot_contact_record(contactID, updateData)

    #Send Notification
    notify_contact_updated(contactID)




def start_hs_write(request):
    #Get Request Data
    data = request.get_json()
    print(data)

    # Trigger Actual Worker Thread
    t = threading.Thread(target=start_write_function , args=(data,))
    t.start()

    # Return 200
    return '', 200
