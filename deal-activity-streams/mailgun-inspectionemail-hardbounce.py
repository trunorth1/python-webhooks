from flask import escape
from requests import get
import requests
import json

def notify_inspectionemail_hardbounce(request):
    """Responds to any HTTP request.
    Args:
        request (flask.Request): HTTP request object.
    Returns:
        The response text or any set of values that can be turned into a
        Response object using
        `make_response <http://flask.pocoo.org/docs/1.0/api/#flask.Flask.make_response>`.
    """
    data = request.get_json()
    refresh_token = '44f0e27e-d42f-4c82-b9fa-9074dd37dd40'
    client_id = '36ce1e45-8c8c-4596-b70f-d4afef4a390a'
    client_secret = '366bc843-dc73-4928-b7bc-07d33c425928'

    write_url = 'https://api.hubapi.com/oauth/v1/token'
    payload = 'grant_type=refresh_token&client_id=' + client_id + '&client_secret=' + client_secret + '&refresh_token=' + refresh_token
    headers = {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}
    resolved = requests.post(write_url, data=payload, headers=headers)
    print(resolved)
    print(resolved.json())
    dataStore = resolved.json()
    #Get Usable Oauth Token
    oauth_token = dataStore['access_token']
    oauth_token = 'Bearer ' + oauth_token
    #Get data from API
    dealID = data['dealID']
    emailaddress = data['emailaddress']
    sender = data['sender']
    deliverystatus = data['deliverystatus']
    print(data)
    #Set Hubspot Event Data
    eventData = {'eventTemplateId' : 1009919, 'objectId': dealID, 'tokens':{'emailaddress' : emailaddress, 'sender' : sender, 'deliverystatus' : deliverystatus}}
    print(eventData)
    #Push Data to Event App
    write_url = 'https://api.hubapi.com/crm/v3/timeline/events'
    payload = eventData
    headers = {'Content-Type': 'application/json', 'Authorization': oauth_token}
    resolved = requests.post(write_url, data=json.dumps(payload), headers=headers)
    print(resolved)
    print(resolved.json())
